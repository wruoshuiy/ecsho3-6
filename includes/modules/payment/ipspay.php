<?php

/**
 * ECSHOP 环迅支付插件
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/ipspay.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'ipspay_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'wy';

    /* 网址 */
    $modules[$i]['website'] = 'http://www.ips.com.cn';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.0';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'MerCode',   'type' => 'text', 'value' => ''),
        array('name' => 'Account',       'type' => 'text', 'value' => ''),
        array('name' => 'MerCert',     'type' => 'textarea', 'value' => '')
    );

    return;
}

/**
 * 类
 */
class ipspay
{
    function __construct()
    {
        $this->ipspay();
    }

    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */
    function ipspay()
    {
    }

    /**
     * 生成支付代码
     * @param   array    $order       订单信息
     * @param   array    $payment     支付方式信息
     */
    function get_code($order, $payment)
    {
//     	var_dump($order);var_dump($payment);exit;
    	
    	require(ROOT_PATH . 'includes/modules/payment/ipssdk/IpsPaySubmit.class.php');
        $cmd_no = '1';

        /* 获得订单的流水号，补零到10位 */
        $sp_billno = $order['order_sn'];

        /* 交易日期 */
        $today = date('Ymd');

        /* 将商户号+年月日+流水号 */
        $bill_no = str_pad($order['log_id'], 10, 0, STR_PAD_LEFT);
        $transaction_id = $today.$bill_no;

        /* 银行类型:支持纯网关和财付通 */
        $bank_type = '0';

//         /* 订单描述，用订单号替代 */
//         if (!empty($order['order_id']))
//         {
//             //$desc = get_goods_name_by_id($order['order_id']);
//             $desc = $order['order_sn'];
//             $attach = '';
//         }
//         else
//         {
//             $desc = $GLOBALS['_LANG']['account_voucher'];
//             $attach = 'voucher';
//         }
//         /* 编码标准 */
//         if (!defined('EC_CHARSET') || EC_CHARSET == 'utf-8')
//         {
//             $desc = ecs_iconv('utf-8', 'gbk', $desc);
//         }

        /* 返回的路径 */
        $return_url = return_url('ipspay');

        /* 总金额 */
        $total_fee = floatval($order['order_amount']) ;



//         /* 交易参数 */
//         $parameter = array(
//             'cmdno'             => $cmd_no,                     // 业务代码, 财付通支付支付接口填  1
//             'date'              => $today,                      // 商户日期：如20051212
//             'bank_type'         => $bank_type,                  // 银行类型:支持纯网关和财付通
//             'desc'              => $desc,                       // 交易的商品名称
//             'purchaser_id'      => '',                          // 用户(买方)的财付通帐户,可以为空
//             'bargainor_id'      => $payment['tenpay_account'],  // 商家的财付通商户号
//             'transaction_id'    => $transaction_id,             // 交易号(订单号)，由商户网站产生(建议顺序累加)
//             'sp_billno'         => $sp_billno,                  // 商户系统内部的定单号,最多10位
//             'total_fee'         => $total_fee,                  // 订单金额
//             'fee_type'          => $fee_type,                   // 现金支付币种
//             'return_url'        => $return_url,                 // 接收财付通返回结果的URL
//             'attach'            => $attach,                     // 用户自定义签名
//             'sign'              => $sign,                       // MD5签名
//             'spbill_create_ip'  => $spbill_create_ip,           //财付通风险防范参数
//             'sys_id'            => '542554970',                 //ecshop C账号 不参与签名
//             'sp_suggestuser'    => '1202822001'                 //财付通分配的商户号

//         );

        
        $ipspay_config = array (
        		"Version" => 'V1.0.0' ,
        		"MerCode" => $this->classConfig [ 'MerCode' ] ,
        		"Account" => $this->classConfig [ 'Account' ] ,
        		"MerCert" => $this->classConfig [ 'MerCert' ] ,
        		'OrderEncodeType' => "5" ,
        		'GatewayType' => "01" ,
        		'RetType' => "1" ,
        		'Ccy' => "156" ,
        		'Lang' => "GB" ,
        		'PostUrl' => 'https://newpay.ips.com.cn/psfp-entry/gateway/payment.do' ,
        		'S2Snotify_url' => $return_url,
        		'return_url' => $return_url ,
        		'RetEncodeType' => "17" ,  // 返回加密方式
        		'GoodsName' => ''
        );
        $parameter = array (
        		"Version" => $ipspay_config [ 'Version' ] ,
        		"MerCode" => $payment [ 'MerCode' ] ,
        		"Account" => $payment [ 'Account' ] ,
        		"MerCert" => $payment [ 'MerCert' ] ,
        		"S2Snotify_url" => $ipspay_config [ 'S2Snotify_url' ] ,
        		"Return_url" => $ipspay_config [ 'return_url' ] ,
        		"CurrencyType" => $ipspay_config [ 'Ccy' ] ,
        		"Lang" => $ipspay_config [ 'Lang' ] ,
        		"Return_url" => $ipspay_config [ 'return_url' ] ,
        		"OrderEncodeType" => $ipspay_config [ 'OrderEncodeType' ] ,
        		"RetType" => $ipspay_config [ 'RetType' ] ,
        		"MerBillNo" =>$order['order_sn'] ,
        		"MsgId" => $ipspay_config [ 'MsgId' ] ,
        		"FailUrl" => $ipspay_config [ 'return_url' ] ,
        		"Date" => date ( "Ymd" ) ,
        		"ReqDate" => date ( "YmdHis" ) ,
        		"Amount" =>  $order['order_amount'] ,
        		"RetEncodeType" => '17' ,
        		'PayType' => $ipspay_config [ 'GatewayType' ] ,
        		"GoodsName" => $order [ 'goods_name' ] .$order['order_sn']
        )
        ;
        
        var_dump($parameter);exit;
        $ipspaySubmit = new IpsPaySubmit ( $ipspay_config );
        $html_text = $ipspaySubmit->buildRequestForm ( $parameter );
        return $html_text;
        exit ();
        
    }

    /**
     * 响应操作
     */
    function respond()
    {
     return true;
    }
}

?>